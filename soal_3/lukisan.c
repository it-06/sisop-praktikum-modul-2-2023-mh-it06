#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#define ull unsigned long long

// Fungsi untuk membuat direktori
void createDirectory(char *dir, char *usrtime) {
    char path[100];
    sprintf(path, "%s/%s", dir, usrtime);

    pid_t folder = fork();
    if (folder < 0) {
        exit(EXIT_FAILURE);
    }

    // Child process untuk melakukan mkdir
    if (folder == 0) {
        char *argfolder[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argfolder);
    }
}

// Fungsi untuk mengompres direktori menjadi zip
void zip_directory(char *dir_name) {
    chdir("..");

    char zip_file[100];
    sprintf(zip_file, "%.90s.zip", dir_name);

    char *zip_argv[] = {"zip", "-qrm", zip_file, dir_name, NULL};
    execv("/usr/bin/zip", zip_argv);
}

// Fungsi untuk mendownload gambar-gambar
void download_files(char *dir_name) {
    chdir(dir_name);

    // Mendownload 15 gambar
    for (int i = 0; i < 15; i++) {
        if (fork() == 0) {
            time_t file_epoch_time;
            struct tm *file_local_time;

            file_epoch_time = time(NULL);
            file_local_time = localtime(&file_epoch_time);

            char url[100];
            // t adalah detik Epoch Unix
            time_t t = time(NULL);

            // Mendownload gambar dengan ukuran (t % 1000) + 50 piksel
            sprintf(url, "https://picsum.photos/%ld", (t % 1000) + 50);

            char pic_name[100];

            // Gambar yang didownload disimpan dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
            strftime(pic_name, 100, "%Y-%m-%d_%H:%M:%S", file_local_time);

            char *dw_argv[] = {"wget", url, "-qO", pic_name, NULL};
            execv("/usr/bin/wget", dw_argv);
        }
        // Mendownload tiap gambar dengan jarak 5 detik
        sleep(5);
    }
}

void cleanup(char *usrtime) {
    // Implementasi fungsi cleanup
    char zip_name[100];
    sprintf(zip_name, "%s.zip", usrtime);
    char *argv[] = {"zip", "-rm", zip_name, usrtime, NULL};
    execv("/usr/bin/zip", argv);
}

void killer(pid_t pid, char param) {
    char cwd[100];
    // Mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;

    // Melakukan fork pada parent
    pid_t parent = fork();
    if (parent < 0) {
        exit(EXIT_FAILURE);
    }

    if (parent == 0) {
        FILE *fp;
        char killer_file[100];
        char data[1000];
        char dest[100];
        char file_name[100];
        char mode[100];

        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);
        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a')
            strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b')
            sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);

        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status) > 0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }

    while (wait(&status) > 0);
    // Melakukan fork pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0)
        exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[100];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }

    while (wait(&status) > 0);
}

int main(int argc, char *argv[]) {
    // Memastikan inputan saat menjalankan berargumen -a atau -b
    if (argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b') {
        printf("Masukkan argumen -a atau -b");
        return 0;
    }

    char dir[100];
    // Mendapatkan direktori kita saat ini
    getcwd(dir, sizeof(dir));

    pid_t pid, sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    // Memanggil fungsi killer
    killer(getpid(), argv[1][1]);
    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    killer(getpid(), argv[1][1]);

    while (1) {
        // Membuat parent process
        pid_t parent;
        time_t waktu;
        char usrtime[100];
        char path[100];
        struct tm *info;
        time(&waktu);
        info = localtime(&waktu);

        strftime(usrtime, 100, "%Y-%m-%d_%X", info);

        sprintf(path, "%s/%s", dir, usrtime);
        // Forking parent
        parent = fork();
        if (parent < 0)
            exit(EXIT_FAILURE);
        if (parent == 0) {
            // Memanggil fungsi createDirectory
            createDirectory(dir, usrtime);

            pid_t donlod;
            int status_donlod;
            donlod = fork();
            if (donlod < 0)
                exit(EXIT_FAILURE);
            if (donlod == 0) {
                // Memanggil fungsi download_files
                download_files(usrtime);
            }
            wait(&status_donlod);
            // Memanggil fungsi zip_directory
            zip_directory(usrtime);
        }
        sleep(30);
    }
}