#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>

// Membuat untuk meng-run executiom
void run_exec(char path[], char *argv[]) {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        execv(path, argv);
        exit(0);
    } else {
        while (wait(&status) > 0);
    }
}

// Mendownload file untuk extension.csv
void downloadFile() {
    char *url = "https://drive.google.com/u/0/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5&export=download";
    char *Namafile = "extensions.csv";
    char *wget_argv[] = { "wget", "--no-check-certificate", url, "-O", Namafile, "-q", NULL }; // Dwonload memakai wget
    run_exec("/usr/bin/wget", wget_argv);
    printf("file extensions.csv berhasil didownload");
}

int quarantine(char *Namafile) {
    char Quarantinefldr[] = "./quarantine/";
    char Quarantinevirus[256];
    snprintf(Quarantinefldr, sizeof(Quarantinevirus), "%s%s", Quarantinefldr, Namafile);

    // Membuat file quarantine
    mkdir(quarantine-antivirus, 0550);

    // Pindahkan file ke folder quarantine
    if (rename(Namafile, Quarantinevirus) == 0) {
        return 1; 
    } else {
        perror("Gagal memindahkan file");
        return 0; // 
    }
}

int isVirusExtension(char *Namafile, char **Virusextensions, int Nomorextensions) {
    char *extension = strrchr(Namafile, '.'); // Mencari '.' extensi di Namafile
    if (extension != NULL) {
        extension++; // '.' != null
        for (int i = 0; i < Nomorextensions; i++) {
            if (strcmp(extension, Virusextensions[i]) == 0) {
                return 1; // File memiliki virus
            }
        }
    }
    return 0; // File tidak memiliki virus 
}

// Mendekripsi menggunakan rot13
void rot13(char *str) {
    char *ptr = str; 
    while (*ptr != '\0') { //meloop karakter
        if ((*ptr >= 'A' && *ptr <= 'M') || (*ptr >= 'a' && *ptr <= 'm')) {
            *ptr += 13; 
        } else if ((*ptr >= 'N' && *ptr <= 'Z') || (*ptr >= 'n' && *ptr <= 'z')) {
            *ptr -= 13; 
        }
        ptr++; 
    }
}

// Mendeteksi virus
void logvirus(char *Namafile, char *action) {
    //Lalu dideklarasi
    char logNamafile[] = "virus.log";
    FILE *logfile = fopen(logNamafile, "a"); 

   
    if (logFile == NULL) {
        printf("Error: log file");
        return;
    }
    
    // Waktu
    time_t waktudetik; //Mendeklarasi variabel waktu
    struct tm *infowaktu; //format waktu
    char waktu[20]; //Char array untuk mendeclare waktu

    time(&waktudetik); //Mengambil waktu detik
    infowaktu = waktulokal(&waktudetik); //Mengambil waktulokal
    strftime(waktu, sizeof(waktu), "%d-%m-%y:%H-%M-%S", infowaktu); //Memformat waktu

    char *username = getlogin(); 
    if (username == NULL) { 
        username = "unknown";
    }

    fprintf(logfile, "[%s][%s] - %s - %s\n", username, waktu, Namafile, action); //Mengeprint format log ke log file
    fclose(logfile); //Menutup log file
}

int main(int argc, char *argv[]) {
    Downloadfile();

    char *Virusextensions[] = { ".virus" }; // Menambahkan ekstensi tambahan jika perlu
    int Nomorextensions = sizeof(Virusextensions) / sizeof(Virusextensions[0]);

    while (1) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir("sisop_infected");
        if (dir == NULL) {
            perror("Gagal membuka sisop_infected"); 
            exit(EXIT_FAILURE);
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) { // Mengecek file
                char *Namafile = entry->d_name;

                if (isVirusExtension(Namafile, Virusextensions, Nomorextensions)) {
                    printf("Mendeteksi virus: %s\n", Namafile);
                    if (quarantine(Namafile)) {
                        printf("Memindahkan file ke quarantine: %s\n", Namafile);
                    }
                }
            }
        }

        closedir(dir);
      }
    return 0;
}