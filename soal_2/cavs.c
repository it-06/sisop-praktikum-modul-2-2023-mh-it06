#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>

int playerCavs(const char* fileName){
    return strstr(fileName, "Cavaliers") != NULL;
}

int main(){
    pid_t second_pid;

    second_pid = fork();

    if(second_pid < 0){
        perror("[!] Fork failed [!]");
        exit(1);
    }

    if(second_pid == 0){
        // Child Process

        // Downloading Database
        pid_t download_pid = fork();

        if(download_pid < 0){
            perror("[!] Fork for download failed [!]");
            exit(1);
        }

        if(download_pid == 0){
            char* const commandWget[] = {"wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL};

            execvp("wget", commandWget);
            perror("[!] Execvp for wget failed [!]");
            exit(1);
        }else{
            int download_status;
            waitpid(download_pid, &download_status, 0);

            if(WIFEXITED(download_status) && WEXITSTATUS(download_status) == 0) {
                printf("[!] Download Success [!]\n");
            }else{
                printf("[!] Download Failed [!]\n");
                exit(1);
            }
        }

        // Extract the database
        pid_t extract_pid = fork();

        if(extract_pid < 0){
            perror("Fork for extraction failed");
            exit(1);
        }

        if(extract_pid == 0){
            char* const commandUnzip[] = {"unzip", "players.zip", "-d", "players", NULL};
            
            execvp("unzip", commandUnzip);
            
            perror("[!] Execvp failed [!]");
            exit(1);
        }else{
            // Parent Process
            int extract_status;
            waitpid(extract_pid, &extract_status, 0);

            if(WIFEXITED(extract_status) && WEXITSTATUS(extract_status) == 0){
                printf("[!] Extract Success [!]\n");
            }else{
                printf("[!] Extract Failed [!]\n");
                exit(1);
            }
        }

        // Deleting the Zip File
        pid_t delete_pid = fork();

        if(delete_pid < 0){
            perror("[!] Fork for deletion failed [!]");
            exit(1);
        }

        if(delete_pid == 0){
            char* const commandRemove[] = {"rm", "players.zip", NULL};
            
            execvp("rm", commandRemove);
            perror("[!] Execvp failed [!]");
            exit(1);
        }else{
            int delete_status;
            waitpid(delete_pid, &delete_status, 0);

            if(WIFEXITED(delete_status) && WEXITSTATUS(delete_status) == 0){
                printf("[!] ZIP File deletion success [!]\n");
            }else{
                printf("[!] ZIP File deletion failed [!]\n");
                exit(1);
            }
        }

        // Categorize Players
        char* playerPosition[] = {"PG", "SG", "SF", "PF", "C"};
        int playerCount[5] = {0};

        DIR* dir;
        struct dirent* entry;

        dir = opendir("players"); 
        if(dir == NULL){
            perror("[!] Failed to open players directory [!]");
            exit(1);
        }

        while((entry = readdir(dir)) != NULL){
            if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0){
                if(PlayerCavs(entry->d_name)){
                    if(strstr(entry->d_name, "PG") != NULL){
                        playerCount[0]++;
                    }else if(strstr(entry->d_name, "SG") != NULL){
                        playerCount[1]++; 
                    }else if(strstr(entry->d_name, "SF") != NULL){
                        playerCount[2]++;
                    }else if(strstr(entry->d_name, "PF") != NULL){
                        playerCount[3]++;
                    }else if(strstr(entry->d_name, "C") != NULL){
                        playerCount[4]++;
                    }
                }
            }
        }

        FILE* appendFormasi = fopen("Formasi.txt", "w");
        if(appendFormasi == NULL){
            perror("[!] Failed to open Formasi.txt for writing [!]");
            closedir(dir);
            exit(1);
        }

        fprintf(appendFormasi, "PG: %d\n", playerCount[0]); 
        fprintf(appendFormasi, "SG: %d\n", playerCount[1]); 
        fprintf(appendFormasi, "SF: %d\n", playerCount[2]); 
        fprintf(appendFormasi, "PF: %d\n", playerCount[3]); 
        fprintf(appendFormasi, "C: %d\n", playerCount[4]); 

        fclose(appendFormasi);
        closedir(dir);

        // Creating the Clutch Folder
        mkdir("clutch", 0755);

        // Moving players to the Clutch Folder
        dir = opendir("players");
        if(dir == NULL){
            perror("[!] Failed to open players directory [!]");
            exit(1);
        }

        while((entry = readdir(dir)) != NULL){
            if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0){
                char filePath[512];
                snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);
                if(PlayerCavs(entry->d_name)){
                    if(strstr(entry->d_name, "LeBron-James") != NULL || strstr(entry->d_name, "Kyrie-Irving") != NULL){
                        char destPath[512];
                        snprintf(destPath, sizeof(destPath), "clutch/%s", entry->d_name);
                        rename(filePath, destPath);
                    }
                }
            }
        }

        closedir(dir);
    }else{
        wait(NULL);
    }
    return 0;
}
