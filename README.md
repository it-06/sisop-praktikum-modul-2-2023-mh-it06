# sisop-praktikum-modul 2-2023-MH-IT06

# Anggota

| Nama                   | NRP  |
|------------------------|------------------|
| Mutiara Nurhaliza     | 5027221010       |
| Rafif Dhimaz Ardhana  | 5027221066       |
| Ahmad Fauzan Daniswara | 5027221057      |

## Daftar Isi

- [Soal 1](#soal-1)
  - [Deskripsi Soal 1](#deskripsi-soal-1)
  - [Penyelesaian Soal 1](#penyelesaian-soal-1)
  - [Output Soal 1](#Output-soal-1)
  - [Revisi Soal 1](#Output-soal-1)
  - [Kendala Soal 1](#Output-soal-1)
- [Soal 3](#soal-3)
  - [Deskripsi Soal 3](#deskripsi-soal-3)
  - [Penyelesaian Soal 3](#penyelesaian-soal-3)
  - [Output Soal 3](#Output-soal-3)
  - [Revisi Soal 3](#Output-soal-3)
  - [Kendala Soal 3](#Output-soal-3)
- [Soal 4](#soal-4)
  - [Deskripsi Soal 4](#deskripsi-soal-4)
  - [Penyelesaian Soal 4](#penyelesaian-soal-4)
  - [Output Soal 4](#Output-soal-4)
  - [Revisi Soal 4](#Output-soal-4)
  - [Kendala Soal 4](#Output-soal-4)

# Soal 1

## Deskripsi Soal

Membuat sebuah program yang dapat menghapus file berisi "SUSPICIOUS" pada directory yang diinputkan user pada saat menjalankan program. Program tersebut akan berjalan secara daemon dan dijalankan setiap interval waktu 30 detik. Program akan masuk ke direktori user dengan fungsi **"argv"** dan mengeluarkan output kepada file log yang berisikan timestamp [YYYY-mm-dd HH:MM:SS] beserta konfirmasi bahwa file yang berisikan "SUSPICIOUS" telah terhapus dari sistem.

## Penyelesaian Soal

### Penggunaan "argv" & daemon

Program akan menerima input dengan menggunakan **"argv"**:

```
const char *inputPath = argv[1];
```

Line code ini memungkinkan user untuk memasukkan input path direktori yang mana program akan dijalankan.
**"*inputPath"** adalah variabel yang akan digunakan untuk menyimpan hasil input untuk path direktori yang diinginkan. Hal ini bisa terjadi dengan adanya fungsi **"argv"** yang akan memanipulasi data input pada saat eksekusi program. 
Dalam kasus ini, program akan dijalankan dengan format input sebagai berikut: 

```
./cleaner /path/to/cleaning_directory
```

**"argv"** disini digunakan untuk memanipulasi data input yang diberikan dengan cara membedakannya sedari spasi pada saat input (setelah "./cleaner" terdapat spasi yang memungkinkan argv menjalankan fungsinya).

Penggunaan daemon pada program ini sangatlah simple. Hanya dengan menambahkan code line-

```
daemon(1,0);
```

-maka program akan berjalan secara daemon. Fungsi **"daemon"** disini akan membuat dan mengurus PID (Process ID), dan SID (Session ID) secara otomatis, tanpa pendeklarasian dari programmer.

### Penghapusan File yang berisi "SUSPICIOUS"

Program akan menghapus file seutuhnya apabila file tersebut mengandung "SUSPICIOUS" didalamnya. Pertama-tama, program akan membuka direktori yang telah diinputkan user. Apabila direktori tersebut gagal untuk dibuka maka akan terdapat error handling yang menyatakan bahwa hal tersebut gagal dilakukan.
```
DIR *dir;
        struct dirent *entry;

        dir = opendir(inputPath);
        if (dir == NULL) {
            perror("Error opening directory");
            return 1;
```
*Bagian ini akan dibahas lebih lanjut pada seksi revisi.

### Pembuatan file log

Program akan mengeluarkan catatan pada log apabila terjadi penghapusan file yang berisi "SUSPICIOUS". Yang akan dilakukan pertama adalah menentukan file untuk melakukan appending catatan log tersebut.

```
FILE *logFile = fopen("cleaner_log/cleaner_log.txt", "a");

    if (logFile == NULL) {
        perror("Error opening log file");
        return 1;
    }
```

Program menetapkan log file pada direktori 'cleaner_log' dengan nama file 'cleaner_log.txt'. Terlihat dalam code snippet bahwa terdapat **"a"** yang berada disebelah penetapan direktori log filenya. Fungsi tersebut berarti FILE akan menjalankan fungsi appending kepada path direktori yang sudah ditetapkan. Terdapat juga error handling apabila log file gagal dibuka untuk melakukan appending.

Konten yang ingin dilakukan appending adalah sebagai berikut:

```
time_t currentTime;
time(&currentTime);
char timeStr[30];
strftime(timeStr, sizeof(timeStr), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));
fprintf(logFile, "[%s] '%s' has been removed.\n", timeStr, filePath);
```

Deklarasikan dahulu untuk timestampnya menggunakan library '<time.h>' agar output catatan log sesuai dengan ketentuan soal. Apabila tidak ada file yang didalamnya berisi "SUSPICIOUS", maka log file akan tetap kosong dan seluruh file yang berada dalam direktori input tidak akan berubah.

### Output File

**Output Log File**
![Output Log File](https://i.imgur.com/0g1ewOL.png)

**Sebelum program dieksekusi**
![Sebelum program dieksekusi](https://i.imgur.com/SWdDMXB.png)

**Setelah program dieksekusi**
![Setelah program dieksekusi](https://i.imgur.com/6UJpqiE.png)

### Revisi Soal

Pada soal ini, sebelumnya terbuat program untuk menghapus line yang mengandung "SUSPICIOUS" pada sebuah file dengan menggunakan temporary file. Karena soal meminta agar program menghapus file yang mengandung "SUSPICIOUS" tersebut dan bukan hanya line yang didalam filenya, maka pada revisi ini terdapat perbaruan kode yang memenuhi ketentuan soal.

```
while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) {
                char filePath[256];
                snprintf(filePath, sizeof(filePath), "%s/%s", inputPath, entry->d_name);

                FILE *file = fopen(filePath, "rb");
                if (file == NULL) {
                    perror("Error opening file");
                    continue;
                }

                char buffer[1024];
                int foundSuspicious = 0;

                size_t bytesRead;
                while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                    if (memmem(buffer, bytesRead, "SUSPICIOUS", 10) != NULL) {
                        foundSuspicious = 1;
                        time_t currentTime;
                        time(&currentTime);
                        char timeStr[30];
                        strftime(timeStr, sizeof(timeStr), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));
                        fprintf(logFile, "[%s] '%s' has been removed.\n", timeStr, filePath);
                        break;  // Exit loop if suspicious content is found
                    }
                }

                fclose(file);

                if (foundSuspicious) {
                    remove(filePath);
                }
            }
        }

    closedir(dir);
    fclose(logfile);
    sleep(30);
```

Pada while loop ini terdapat perubahan cukup signifikan dimana tidak lagi dipergunakan sistem temporary file untuk membaca file-file yang terdapat dalam input direktori, namun program akan membaca biner dari file tersebut (ditandai dengan fungsi **"rb"**) untuk menentukan apakah file tersebut memiliki "SUSPICIOUS" didalamnya. Apabila file tersebut memiliki "SUSPICIOUS" maka program akan menandainya dengan memberikan value true pada variabel 'foundSuspicious'. Variabel ini akan dipakai untuk menghapus file yang ditemukan ada "SUSPICIOUS" didalamnya.

### Kendala

Tidak ada

# Soal 3

## Deskripsi Soal 

Membuat sebuah program yang bisa berjalan dalam dua mode, MODE_A dan MODE_B, dan membantu seniman terkenal Albedo untuk mengatasi creativity block-nya. Program ini akan membuat folder dengan nama timestamp setiap 30 detik dan mengisi setiap folder dengan 15 gambar yang diunduh dari sumber tertentu. Gambar-gambar tersebut harus berbentuk persegi dengan ukuran tertentu yang dihitung berdasarkan detik Epoch Unix. Setelah sebuah folder terisi, folder tersebut akan di-zip dan dihapus, meninggalkan hanya file zip dengan nama timestamp.

Selain itu, program ini harus mampu menghasilkan "program killer" yang dapat digunakan untuk menghentikan semua operasi program tersebut. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Sedangkan di MODE_B, program utama akan berhenti, tetapi membiarkan proses di setiap folder yang masih berjalan sampai selesai.
## Penyelesaian Soal

### Point A

Membuat sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].


```
void createDirectory(char *dir, char *usrtime) {
    char path[100];
    sprintf(path, "%s/%s", dir, usrtime);

    pid_t folder = fork();
    if (folder < 0) {
        exit(EXIT_FAILURE);
    }

    // Child process untuk melakukan mkdir
    if (folder == 0) {
        char *argfolder[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argfolder);
    }
}

```

**`createDirectory`** adalah sebuah fungsi yang digunakan untuk membuat direktori baru dengan menggunakan perintah "mkdir". Fungsi ini menerima dua parameter: dir yang merupakan direktori utama (parent directory) dan usrtime yang merupakan timestamp yang akan digunakan sebagai nama direktori baru.

**`Fungsi sprintf`** digunakan untuk menggabungkan dir dan usrtime menjadi satu string yang akan menjadi path lengkap direktori yang akan dibuat.

**`Fungsi fork`** digunakan untuk membuat child process yang akan menjalankan perintah "mkdir". Jika fork mengembalikan nilai kurang dari 0, maka proses fork gagal, dan program akan keluar dengan status kegagalan.

**`Child process`** (ketika folder == 0) akan menjalankan perintah "mkdir" dengan argumen yang sudah disiapkan. Perintah "mkdir -p" digunakan untuk membuat direktori dan direktori parent jika belum ada.

**`Perintah execv`** digunakan untuk menjalankan perintah "mkdir" dengan argumen yang telah disiapkan. Ini akan menggantikan proses child dengan proses "mkdir", dan ketika perintah "mkdir" selesai, child process akan selesai juga.

### Point B

Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].


```
void download_files(char *dir_name) {
    chdir(dir_name);

    // Mendownload 15 gambar
    for (int i = 0; i < 15; i++) {
        if (fork() == 0) {
            time_t file_epoch_time;
            struct tm *file_local_time;

            file_epoch_time = time(NULL);
            file_local_time = localtime(&file_epoch_time);

            char url[100];
            // t adalah detik Epoch Unix
            time_t t = time(NULL);

            // Mendownload gambar dengan ukuran (t % 1000) + 50 piksel
            sprintf(url, "https://picsum.photos/%ld", (t % 1000) + 50);

            char pic_name[100];

            // Gambar yang didownload disimpan dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
            strftime(pic_name, 100, "%Y-%m-%d_%H:%M:%S", file_local_time);

            char *dw_argv[] = {"wget", url, "-qO", pic_name, NULL};
            execv("/usr/bin/wget", dw_argv);
        }
        // Mendownload tiap gambar dengan jarak 5 detik
        sleep(5);
    }
}

```

**`void download_files(char \*dir_name\)`**  Deklarasi fungsi download_files yang menerima satu argumen, yaitu nama direktori (dir_name) di mana gambar-gambar akan disimpan.

**`chdir(dir_name)`** Mengganti direktori kerja saat ini ke direktori yang diberikan dalam argumen dir_name. Sehingga semua gambar yang diunduh akan disimpan dalam direktori yang telah ditentukan.

**`for (int i = 0; i < 15; i++)`** Loop for akan berjalan sebanyak 15 kali untuk mengunduh 15 gambar.

**`if (fork() == 0)`** Pemanggilan fork(). Saat pemanggilan  dijalankan, sebuah proses anak akan dibuat. Kode yang ada di dalam blok ini akan dijalankan oleh proses anak.

**`time_t file_epoch_time;
struct tm \*file_local_time\;`**
Variabel file_epoch_time digunakan untuk menyimpan waktu dalam format epoch Unix, sedangkan file_local_time adalah pointer ke struktur tm yang akan digunakan untuk menyimpan waktu dalam format lokal.

**`file_epoch_time = time(NULL);`** Waktu saat ini dalam format epoch Unix diambil dengan menggunakan fungsi time(NULL) dan disimpan dalam file_epoch_time.

**`file_local_time = localtime(&file_epoch_time);`** Waktu dalam format epoch Unix kemudian diubah menjadi waktu lokal dan disimpan dalam file_local_time.

**`char url[100];`** Array karakter yang akan digunakan untuk menyimpan URL gambar yang akan diunduh. Kapasitasnya adalah 100 karakter.

**`time_t t = time(NULL);`** Waktu saat ini dalam format epoch Unix diambil lagi dan disimpan dalam variabel t.

 **`sprintf(url, "https://picsum.photos/%ld", (t % 1000) + 50);`** URL gambar dibuat dengan menggabungkan string konstan "https://picsum.photos/" dengan nilai (t % 1000) + 50. Ini akan menghasilkan URL yang berbeda setiap kali loop dijalankan, berdasarkan waktu saat itu.

**`char pic_name[100];`** Array karakter yang akan digunakan untuk menyimpan nama file gambar yang akan diunduh.

**`strftime(pic_name, 100, "%Y-%m-%d_%H:%M:%S", file_local_time);`** Format waktu lokal dalam file_local_time diubah menjadi string dengan format "YYYY-mm-dd_HH:mm:ss" dan disimpan dalam pic_name.

**`char \*dw_argv[] = {"wget", url, "-qO", pic_name, NULL};`** Array yang akan digunakan sebagai argumen untuk menjalankan perintah wget. Akan mengunduh gambar dari URL yang telah dibuat dan menyimpannya dengan nama yang sesuai.

**`execv("/usr/bin/wget", dw_argv);`** Fungsi execv digunakan untuk menjalankan perintah wget dengan argumen yang telah diatur sebelumnya. Mengunduh gambar dari URL dan menyimpannya dengan nama yang sesuai dalam direktori yang telah ditentukan.

**`sleep(5);`** Setelah mengunduh satu gambar, proses anak akan tidur selama 5 detik sebelum mengunduh gambar berikutnya. 

### Point C

Setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).


```
void zip_directory(char *dir_name) {
    chdir("..");

    char zip_file[100];
    sprintf(zip_file, "%.90s.zip", dir_name);

    char *zip_argv[] = {"zip", "-qrm", zip_file, dir_name, NULL};
    execv("/usr/bin/zip", zip_argv);
}
```

**`chdir(".."):`** Menggunakan chdir("..") untuk mengganti direktori kerja saat ini menjadi direktori di atasnya (parent directory). Untuk memastikan bahwa direktori yang akan di-zip adalah direktori dengan nama dir_name yang diberikan sebagai parameter.

**`char zip_file[100]:`** Variabel zip_file dideklarasikan sebagai array karakter yang akan digunakan untuk menyimpan nama file zip yang akan dibuat. Format nama file zip yang digunakan adalah "[dir_name].zip" dengan maksimal panjang nama sebanyak 90 karakter.

**`sprintf(zip_file, "%.90s.zip", dir_name):`** Menggunakan fungsi sprintf untuk menggabungkan nama direktori yang diberikan sebagai parameter (dir_name) dengan ekstensi ".zip" dan menyimpan hasilnya dalam variabel zip_file. 

**`char *zip_argv[]:`** Array pointer karakter yang akan digunakan sebagai argumen untuk menjalankan perintah zip dengan menggunakan execv.

**`execv("/usr/bin/zip", zip_argv):`** Execv untuk menjalankan perintah zip dengan argumen yang telah disiapkan dalam array zip_argv. Perintah zip akan mengompres direktori yang sesuai dengan nama yang diberikan sebagai dir_name ke dalam file zip dengan nama zip_file. Jika operasi ini berhasil, maka direktori yang di-zip akan terkompres dan file asli akan dihapus jika opsi -m digunakan. Jika operasi ini gagal, maka fungsi akan keluar tanpa memberikan pesan kesalahan, jadi pastikan Anda menangani kesalahan dengan baik jika diperlukan.

### Point D dan E

Program memiliki dua mode, yaitu MODE_A dan MODE_B. Dalam MODE_A, program akan berhenti segera setelah program "killer" dijalankan, yang akan menterminasi semua operasi program utama. Sementara itu, dalam MODE_B, program utama akan berhenti, tetapi memungkinkan proses di setiap folder yang sedang berjalan untuk selesai, termasuk mengisi gambar, meng-zip, dan menghapus folder. Untuk mengendalikan program "killer," program utama juga akan menghasilkan program "killer" yang bisa dijalankan sebagai executable untuk menghentikan seluruh operasi, dan setelah digunakan, program "killer" akan menghapus dirinya sendiri. Ini adalah langkah-langkah yang diambil untuk menjaga kendali program dan mengatasi kekhawatiran Albedo.

```
void killer(pid_t pid, char param) {
    char cwd[100];
    // Mendapatkan direktori saat ini
    getcwd(cwd, sizeof(cwd));
    int status;

    // Melakukan fork pada parent
    pid_t parent = fork();
    if (parent < 0) {
        exit(EXIT_FAILURE);
    }

    if (parent == 0) {
        FILE *fp;
        char killer_file[100];
        char data[1000];
        char dest[100];
        char file_name[100];
        char mode[100];

        sprintf(file_name, "%s/killer.c", cwd);
        sprintf(dest, "%s/killer", cwd);
        sprintf(killer_file, "%s/killer.c", cwd);

        if (param == 'a')
            strcpy(mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (param == 'b')
            sprintf(mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);

        sprintf(data,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t child_id;"
                        " int status;"

                        " child_id = fork();"
                        " if (child_id < 0) exit(EXIT_FAILURE);"

                        " if (child_id == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status) > 0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", mode, cwd);

        fp = fopen(killer_file, "w");
        fputs(data, fp);
        fclose(fp);

        char *argv[] = {"gcc", file_name, "-o", dest, NULL};
        execv("/usr/bin/gcc", argv);
    }

    while (wait(&status) > 0);
    // Melakukan fork pada remove_killer
    pid_t remove_killer = fork();
    if (remove_killer < 0)
        exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char src[100];
        sprintf(src, "%s/killer.c", cwd);
        char *argv[] = {"rm", src, NULL};
        execv("/bin/rm", argv);
    }

    while (wait(&status) > 0);
}


```
Fungsi ini menghasilkan dua program "killer", satu untuk menghentikan proses (param == 'a') dan yang lainnya untuk menghapus program "killer" itu sendiri setelah selesai (param == 'b'). 

**`char cwd[100]`** Variabel cwd digunakan untuk menyimpan direktori kerja saat ini 

**`getcwd(cwd, sizeof(cwd))`** getcwd untuk mendapatkan direktori kerja saat ini dan menyimpannya dalam variabel cwd.

**`pid_t parent = fork()`** Melakukan fork untuk menciptakan proses anak (child process). 


Di dalam child process, beberapa variabel lainnya dideklarasikan:  
**`killer_file`** untuk menyimpan nama file program "killer" yang akan dibuat.  
**`data`** untuk menyimpan isi dari program "killer" yang akan ditulis ke dalam file.  
**`dest`** untuk menyimpan lokasi tempat program "killer" akan dibuat.  
**`file_name`** untuk menyimpan nama file program "killer" dalam format C.  
**`mode`** akan berisi kode C yang akan dijalankan oleh program "killer" berdasarkan parameter param.

Berdasarkan nilai param, variabel mode akan diisi dengan kode C yang sesuai:  
Jika **`param == 'a'`**, maka program "killer" akan dibuat untuk menghentikan semua proses dengan nama "lukisan" menggunakan killall.  
Jika **`param == 'b'`**, maka program "killer" akan dibuat untuk menghentikan proses dengan pid yang diberikan menggunakan kill.

Variabel data akan diisi dengan kode C lengkap yang akan menjadi isi dari program "killer". Kode ini mencakup pembuatan proses anak (child process) dan jalannya perintah mode. Setelah itu, kode C akan menghapus program "killer" itu sendiri.

Program "killer" akan ditulis ke dalam file killer_file menggunakan fungsi fopen, fputs, dan fclose.
Setelah kode program "killer" ditulis ke dalam file, sebuah program kompilasi untuk mengkompilasi file C "killer.c" menjadi program eksekusi "killer" dilakukan dengan menggunakan gcc melalui execv.

Jika proses kompilasi selesai, child process akan selesai, dan parent process akan menunggu hingga child process selesai dengan menggunakan wait.

Ketika program "killer" selesai kompilasi, program parent process akan melakukan fork lagi untuk menciptakan proses "remove_killer".

Pada proses "remove_killer", file "killer.c" yang digunakan untuk membuat program "killer" akan dihapus dari sistem menggunakan perintah rm melalui execv.

## Output Mode A

**Membuat File**
![Membuat File](https://i.imgur.com/E56Z2xu.jpg)

**Mengunduh Gambar**
![Mengunduh Gambar](https://i.imgur.com/PUeAWid.jpg)

**Melakukan Zip**
![Melakukan Zip](https://i.imgur.com/G1uYBVm.jpg)

**Memanggil Killer -a**
![Memanggil Killer -a](https://i.imgur.com/Mjh4EHy.jpg)

## Output Mode B

**Membuat File**
![Membuat File](https://i.imgur.com/TMICmYV.jpg)

**Menngunduh Gambar**
![Mengunduh Gambar](https://i.imgur.com/9Wqeldz.jpg)

**Melakukan Zip**
![Melakukan Zip](https://i.imgur.com/SPD4kc3.jpg)

**Memanggil Killer -b**
![Memanggil Killer -b](https://i.imgur.com/YDv7Wn7.jpg)

## Revisi

Tidak ada

## Kendala

Tidak ada

# Soal 4

## Deskripsi Soal 4

Pada soal nomor 4 praktikan diminta untuk meningkatkan kinerja dari sebuah program yang telah dibuat yaitu antivirus.c untuk melindungi data. Praktikan diminta untuk mendownload file extensions.csv yang didalamnya terdapat ekstensi file yang dianggap virus, jika file diidentifikasi sebagai virus berdasarkan ekstensi, harus dipindahkan ke folder quarantine melalui program. Kemudian terdapat 8 baris pertama yang tidak terenkripsi sehingga harus dienkripsi menggunakan algoritma rot13 dan setiap program mendeteksi virus,catat di virus.log dengan format yang sudah ditentukan. kemudian praktikan diminta untuk bisa menjalankan programnya di latar belakang tanpa intervensi yang mana programnya bisa secara otomatis memeriksa folder sisop_infected setiap detiknya. Lalu praktikan diminta untuk membuat level-level keamanan antivirus berdasarkan 3 level yaitu low,medium,hard yang bisa dipakai saat menjalankan antivirus dengan format yang sudah ditentukan, namun praktikan juga diharuskan mengganti level keamanan dari antivirusnya tanpa harus menghentikan antivirusnya lalu antivirus juga harus bisa dinonaktifkan dengan efisien.

## Penyelesaian Soal 4

### A. Fungsi untuk menjalankan program dan juga fungsi untuk mendownload file extensions.csv

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h> //fungsi yang dipakai saat pengerjaan soal 4
```

```
void run_exec(char path[], char *argv[]) {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        execv(path, argv);
        exit(0);
    } else {
        while (wait(&status) > 0);
    }
}
```
fungsi ini adalah fungsi untuk bisa mengerun execution agar bisa mendownload file extensions.csv nya

```
void downloadFile() {
    //declare variables utnuk downloaded file dan file name
    char *url = "https://drive.google.com/u/0/uc?id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5&export=download";
    char *fileName = "extensions.csv";
    // Mengdownload pakai wget lalu wget dijalankan
    char *wget_argv[] = { "wget", "--no-check-certificate", url, "-O", fileName, "-q", NULL };
    run_exec("/usr/bin/wget", wget_argv);
    printf("File extension.csv berhasil didownload");
}
```
fungsi ini adalah fungsi yang dipakai untuk mendownload file exstensions.csv

### B. Membuat fungsi yang berkaitan dengan Quarantine mulai dari mengdeclare, membuat folder quarantine, lalu memindahkan file yang memiliki virus ke Folder quarantine

```
int Quarantine(char *fileName) {
    char quarantineFolder[] = "./quarantine/";
    char quarantinePath[256];
    snprintf(quarantinePath, sizeof(quarantinePath), "%s%s", quarantineFolder, fileName);
```
Fungsi ini dipakai untuk mendeclare file yang akan dipindahkan ke quarantine

```
    // BUat folder quarantine
    mkdir(quarantineFolder, 0755);

    // Pindahkan file ke folder quarantine
    if (rename(fileName, quarantinePath) == 0) {
        return 1; // File sukses dipindahkan
    } else {
        perror("Gagal memindahkan file ke folder quarantine");
        return 0; // File gagal dipindahkan
    }
}
```
Fungsi ini dipakai untuk membuat folder quarantine dan kemudian memindahkan file yang memiliki virus kedalam folder quarantine

### C. Membuat fungsi untuk mendeteksi virus yang ada di file 

```
int isVirusExtension(char *fileName, char **virusExtensions, int numExtensions) {
    char *extension = strrchr(fileName, '.'); // Mencari '.' extensi di filename
    if (extension != NULL) {
        extension++; // jika '.' != null
        for (int i = 0; i < numExtensions; i++) {
            if (strcmp(extension, virusExtensions[i]) == 0) {
                return 1; // File terjangkit ekstensi virus
            }
        }
    }
    return 0; // File tdk terjangkit ekstensi virus
}
```

### D. Membuat fungsi untuk mendecrypt file setelah 8 baris pertama memakai algoritma rot13 lalu juga buat fungsi untuk mencatat deteksi file virus di virus.log

```
void rot13(char *str) {
    char *ptr = str; 
    while (*ptr != '\0') { 
        if ((*ptr >= 'A' && *ptr <= 'M') || (*ptr >= 'a' && *ptr <= 'm')) {
            *ptr += 13; 
        } else if ((*ptr >= 'N' && *ptr <= 'Z') || (*ptr >= 'n' && *ptr <= 'z')) {
            *ptr -= 13; 
        }
        ptr++; 
    }
}
```
Fungsi ini adalah fungsi untuk mendekripsi file yang harus didekripsi dengan memakai algoritma ROT13

```
// Mendeteksi log virus 
void logVirus(char *fileName, char *action) {
    //Deklarasikan variabelnya
    char logFileName[] = "virus.log";
    FILE *logFile = fopen(logFileName, "a"); 

    //cek apakah file dapat diakses atau tidak (debuging)  
    if (logFile == NULL) {
        printf("Error: log file");
        return;
    }
 ```
 Fungsi ini dipakai untuk mencatat deteksi file ke virus.log

 ### E. Membuat fungsi untuk format virus.log ke format yang sudah ditentukan yaitu [nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

 ```
 time_t rawtime; //declare variabel date datatype
    struct tm *timeinfo; 
    char timestamp[20]; 

    time(&rawtime); 
    timeinfo = localtime(&rawtime); //ambil local time
    strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", timeinfo); //format timestamp ke tanggal, bulan, tahun: jam, bulan, detik; 

    char *username = getlogin(); 
    if (username == NULL) { 
        username = "unknown";
    }

    fprintf(logFile, "[%s][%s] - %s - %s\n", username, timestamp, fileName, action); //print format log ke logfile
    fclose(logFile); 
}
```
Fungsi diatas digunakan untuk memformat virus.log ke format yang ditentukan

 ### F. Membuat fungsi untuk mengecek kembali program apakah bisa berjalan

 ```
 int main(int argc, char *argv[]) {
    downloadFile();

    char *virusExtensions[] = { ".virus" }; // Tambahkan ekstensi jika dibutuhkan
    int numExtensions = sizeof(virusExtensions) / sizeof(virusExtensions[0]);

    while (1) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir("sisop_infected");
        if (dir == NULL) {
            perror("Gagal membuka folder sisop_infected");
            exit(EXIT_FAILURE);
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type == DT_REG) { // Cek apabila ini adalah file reguler
                char *fileName = entry->d_name;

                if (isVirusExtension(fileName, virusExtensions, numExtensions)) {
                    printf("Virus yg terdeteksi: %s\n", fileName);
                    if (Quarantine(fileName)) {
                        printf("Pindahkan ke quarantine: %s\n", fileName);
                    }
                }
            }
        }

        closedir(dir);
       }
    return 0;
}
```

## Output Soal 4
![Output Soal 4](https://i.imgur.com/7heALgr.png)
File extensions.csv berhasil diunduh namun tidak bisa membuka folder sisop_infected

## Revisi Soal 4
Sampai saat ini saya masih belum bisa mengsolve permasalahan yang saya dapat mengenai tidakjalannya fungsi untuk membuka folder sisop sisop_infected

## Kendala Soal 4
Untuk kendala saya ada banyak, yaitu kegagalan untuk membuka folder sisop_infected, yang dimana hal itu menyebabkan fungsi yang dimasukkan tidak bisa berjalan, selain itu poin bagian c,d,e belum sempat terselesaikan karena sudah memasuki deadline.
