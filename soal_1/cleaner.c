#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]){
    const char *inputPath = argv[1];
    
    daemon(1,0);

    FILE *logFile = fopen("cleaner_log/cleaner_log.txt", "a");

    if(logFile == NULL){
        perror("Error opening log file");
        return 1;
    }

    while(1){
        DIR *dir;
        struct dirent *entry;

        dir = opendir(inputPath);
        if(dir == NULL){
            perror("Error opening directory");
            return 1;
        }

        while((entry = readdir(dir)) != NULL){
            if(entry->d_type == DT_REG){
                char filePath[256];
                snprintf(filePath, sizeof(filePath), "%s/%s", inputPath, entry->d_name);

                FILE *file = fopen(filePath, "r");
                if(file == NULL){
                    perror("Error opening file");
                    continue;
                }

                char tempFilePath[256];
                snprintf(tempFilePath, sizeof(tempFilePath), "%s/%s_temp", inputPath, entry->d_name);
                FILE *tempFile = fopen(tempFilePath, "w");
                if(tempFile == NULL){
                    perror("Error creating temporary file");
                    fclose(file);
                    continue;
                }

                char buffer[1024];
                int foundSuspicious = 0;

                while(fgets(buffer, sizeof(buffer), file) != NULL){
                    if(strstr(buffer, "SUSPICIOUS") != NULL){
                        foundSuspicious = 1;
                        time_t currentTime;
                        time(&currentTime);
                        char timeStr[30];
                        strftime(timeStr, sizeof(timeStr), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));
                        fprintf(logFile, "[%s] '%s' has been removed.\n", timeStr, filePath);
                    }else{
                        fputs(buffer, tempFile);
                    }
                }

                fclose(file);
                fclose(tempFile);

                if(foundSuspicious){
                    remove(filePath);
                    rename(tempFilePath, filePath);
                }else{
                    remove(tempFilePath);
                }
            }
        }

        closedir(dir);
        fclose(logFile);
        sleep(30);
    }
    return 0;
}

